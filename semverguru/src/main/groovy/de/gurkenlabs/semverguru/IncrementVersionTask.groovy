package de.gurkenlabs.semverguru

import org.ajoberstar.grgit.Grgit
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option

import static de.gurkenlabs.semverguru.SemVerUtilities.Increment

abstract class IncrementVersionTask extends DefaultTask {
    private Increment increment;
    @Optional
    private String referenceTag;
    @Optional
    private String tagMessage;

    @TaskAction
    def incrementVersion() {
        def referenceVersion = referenceTag != null ? referenceTag : project.version.toString().split('-')[0]
        def newTag = SemVerUtilities.incrementVersion(referenceVersion, increment)
        Grgit grgit = Grgit.open(dir: project.getProjectDir())
        grgit.tag.add {
            name = newTag
            message = tagMessage != null ? tagMessage : "Release of $newTag"
        }
        project.version = newTag
        getLogger().lifecycle("Incremented {} version from {} to {}.", increment, referenceVersion, newTag)
    }

    @Option(option = "increment", description = "Configures the version increment.")
    void setIncrement(String increment) {
        this.increment = Increment.valueOf(increment.toUpperCase())
    }

    @Option(option = "referenceTag", description = "Configures whether a specific tag is to be incremented instead of the most recent tag.")
    void setReferenceTag(String referenceTag) {
        this.referenceTag = referenceTag
    }

    @Option(option = "tagMessage", description = "Configures the annotation for the new git tag.")
    void setTagMessage(String tagMessage) {
        this.tagMessage = tagMessage
    }

    @Input
    Increment getIncrement() {
        return increment;
    }

    @Input
    String getReferenceTag() {
        return referenceTag
    }

    @Input
    String getTagMessage() {
        return tagMessage
    }

}
