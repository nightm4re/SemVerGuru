package de.gurkenlabs.semverguru

import org.ajoberstar.grgit.Grgit
import org.gradle.api.Project

class SemVerUtilities {
    static boolean hasUnstagedChanges(Grgit grgit) {
        !grgit.diff().isEmpty()
    }

    static boolean isTaggedRevision(String version) {
        def matcher = version =~ /^(\d+\.\d+\.\d+).*/
        matcher.find() && matcher.group(1) == version.toString()
    }

    static enum Increment {
        MAJOR, MINOR, PATCH
    }

    static String incrementVersion(String referenceVersion, Increment increment) {
        int incrementTypeIndex = increment.ordinal()
        def versionParts = referenceVersion.tokenize('.')*.asType(Integer)
        versionParts[incrementTypeIndex] += 1
        for (int i = incrementTypeIndex + 1; i < 3; i++) {
            if (i > incrementTypeIndex) {
                versionParts[i] = 0
            }
        }
        return versionParts.join('.')
    }

    static void determineDevelopmentVersion(Project project) {
        Grgit grgit = Grgit.open(dir: project.getProjectDir())
        def developmentVersion = grgit.describe(tags: true, always: true)
        project.getLogger().lifecycle("Detected development version '$developmentVersion' via git describe.")
        def versionParts = developmentVersion.split('-')
        def mostRecentTag = versionParts[0]
        // if ref is tagged, use the output of git describe
        if (isTaggedRevision(developmentVersion) || !hasUnstagedChanges(grgit)) {
            project.version = developmentVersion
        } else {
            def commitCount = versionParts.length > 1 ? versionParts[1] as Integer + 1 : 1
            project.version = "$mostRecentTag-$commitCount-unstaged"
        }

        project.getLogger().lifecycle("Development version: {}", project.version)
    }

    static void setVersionRecursively(Project project) {
        project.subprojects.each { it ->
            it.version = it.rootProject.version
        }
    }

}
