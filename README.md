# SemVerGuru
![pipelineBadge](https://ci.codeberg.org/api/badges/12644/status.svg)

"SemVerGuru" is your go-to Gradle plugin for effortless version management. Powered by Grgit and git describe, it seamlessly derives your project's version from Git tags. Take control of your versioning journey with easy Semantic Versioning increments, and with one-click git tag pushing. Keep it simple, stay semantic, and let SemVerGuru handle your project versions with ease.

![semverguru-logo](semverguru/src/main/resources/semverguru-logo-small.png)
## Consuming the plugin
This project produces a Gradle plugin with the Plugin ID `de.gurkenlabs.semverguru`.
Head over to the [Gradle Plugin portal](https://plugins.gradle.org/plugin/de.gurkenlabs.semverguru) for details on applying the plugin.
Apply the plugin to your project's root `build.gradle`. 

## Configuring the plugin
At the moment, there are no configurations exposed for this plugin.

## Determining the project version
Once applied to a project, this plugin sets the project version using git describe.
* If a git tag exists at HEAD, the tag becomes the project version.
* If there have been commits since the last tag, the number of commits will be appended to the project version, followed by the abbreviated object name of the most recent commit.
* If there are unstaged changes, the number of commits is incremented by one and the commit reference is set to `'unstaged'`.

SemVerGuru also registers the `isTaggedRevision` extension on your project. It returns `true` if a given String is a valid Semantic Version tag and `false` if not. You can use it, for instance, to modify (un)tagged versions only or determine target repositories for publishing automatically:
```groovy
// build.gradle
plugins {
    id "maven-publish"
}

// append "-SNAPSHOT" to the version string if it's not a valid semantic version tag
allprojects {
  project.version += isTaggedRevision(project.version) ? "" : "-SNAPSHOT"
  project.getLogger().lifecycle("Set project '{}' version to '{}'.", project.name, project.version)
}

// determine target repository for publishing based on whether the project version is a valid semantic version tag.
publishing {
    repositories {
        maven {
            def releasesRepoUrl = layout.buildDirectory.dir('repos/releases')
            def snapshotsRepoUrl = layout.buildDirectory.dir('repos/snapshots')
            url = version.endsWith('SNAPSHOT') ? snapshotsRepoUrl : releasesRepoUrl
        }
    }
}


```

### Examples
#### Example 1
[![](https://mermaid.ink/img/pako:eNptj08LwjAMxb9KydnJ5h90PQsqeNNjL7HNtqLtRk0PIn53O0UdaCCX5PfeS26gW0MgQfna8jpg1ygvXqVb5ywLa6RQkGf5rCxMVSr4DxTZfEnT-WwxAI4BvW5ERcgx0FfXkD61kX8XQ8NJNqlSZrkcJr6VDq3_TB2Fmt5mgrF-HjwuxrkCwdeOpNhs15td6gOMIOFJbtLTt95CATfkSEGvMhhOfd49cRi53V-9Bskh0ghiZ5BpZbEO6EBWeL7Q_QGZM2Bp?type=png)](https://mermaid.live/edit#pako:eNptj08LwjAMxb9KydnJ5h90PQsqeNNjL7HNtqLtRk0PIn53O0UdaCCX5PfeS26gW0MgQfna8jpg1ygvXqVb5ywLa6RQkGf5rCxMVSr4DxTZfEnT-WwxAI4BvW5ERcgx0FfXkD61kX8XQ8NJNqlSZrkcJr6VDq3_TB2Fmt5mgrF-HjwuxrkCwdeOpNhs15td6gOMIOFJbtLTt95CATfkSEGvMhhOfd49cRi53V-9Bskh0ghiZ5BpZbEO6EBWeL7Q_QGZM2Bp)

* Tagged revision 0.1.0
* *Project version determined by SemVerGuru:* **0.1.0**
#### Example 2 
[![](https://mermaid.ink/img/pako:eNp1kD1vwyAQhv8KujlEduqPwFwpqdStHVmucNgoBVsUhijKfy9uldZSm5NY7p73Q1xAT4ZAggqDS4eI86gC-x49ee8Sc0YyBRWvGlEbKxT8D9S83dND2_Qr4C1i0COzhClH-tWNpE9TTn8Pa8Md39mSKfbrxJvSows_W09xoJsZSzh8Fd7W2-pe2YZb3ZHW_T2g5aLvehSdApbOM0l2fDocn8t7VQE2UCJLBVM-7rIYKEgjeVKwaA3G0-J7LRzmNL2cgwaZYqYN5NlgokeHQ0QP0uL7B10_ATUzcvA?type=png)](https://mermaid.live/edit#pako:eNp1kD1vwyAQhv8KujlEduqPwFwpqdStHVmucNgoBVsUhijKfy9uldZSm5NY7p73Q1xAT4ZAggqDS4eI86gC-x49ee8Sc0YyBRWvGlEbKxT8D9S83dND2_Qr4C1i0COzhClH-tWNpE9TTn8Pa8Md39mSKfbrxJvSows_W09xoJsZSzh8Fd7W2-pe2YZb3ZHW_T2g5aLvehSdApbOM0l2fDocn8t7VQE2UCJLBVM-7rIYKEgjeVKwaA3G0-J7LRzmNL2cgwaZYqYN5NlgokeHQ0QP0uL7B10_ATUzcvA)

* Untagged revision, 2 commits since last tag 0.1.0
* *Project version determined by SemVerGuru:* **0.1.0-2-9767a96**

#### Example 3 
[![](https://mermaid.ink/img/pako:eNp1kMtuwyAQRX8FzTqO7NSPmHWlJFJ27ZLNFAZsJWALwyKK8u_FrdJaao3Ehjlz7hV3kIMi4CCc6cPB49gJx76PHKztA-sVZwLyLC_bQulWwP9AkVV7eqnKZgF8eHSyY5owRE-_ex3JyxDD38FSuMt2OmW2-2Xic9Ni735eLXlDTxkLaL4Kb4ttvla2zLSsScpmDaiytqkbbOs1ILopBZFKldAZmgSwcBuJs-PpcDyn-y4cbCBVS1VV-uD77BEQOrIkYFYo9JdZ_0gcxjC83ZwEHnykDcRRYaDXHo1HC1zjdaLHJ-zBf7o?type=png)](https://mermaid.live/edit#pako:eNp1kMtuwyAQRX8FzTqO7NSPmHWlJFJ27ZLNFAZsJWALwyKK8u_FrdJaao3Ehjlz7hV3kIMi4CCc6cPB49gJx76PHKztA-sVZwLyLC_bQulWwP9AkVV7eqnKZgF8eHSyY5owRE-_ex3JyxDD38FSuMt2OmW2-2Xic9Ni735eLXlDTxkLaL4Kb4ttvla2zLSsScpmDaiytqkbbOs1ILopBZFKldAZmgSwcBuJs-PpcDyn-y4cbCBVS1VV-uD77BEQOrIkYFYo9JdZ_0gcxjC83ZwEHnykDcRRYaDXHo1HC1zjdaLHJ-zBf7o)

* Untagged revision, 2 commits since last tag 0.1.0, unstaged changes

* *Project version determined by SemVerGuru:* **0.1.0-3-unstaged**

## Incrementing versions
SemVerGuru provides the `incrementVersion` Gradle task to your project (located in the `versioning` task group).
The `incrementVersion` task bumps up the project version and generates a fresh git tag reflecting the updated version.
You can configure the task's behaviour via the following options:
* `increment` (mandatory; choose one of `"major"`, `"minor"`, or `"patch"`)
* `referenceTag` (optional; choose any semantic version to increment other versions than the latest tag)
* `tagMessage` (optional; provide a short description that will be used as tag annotation)

### Example
* Input: `./gradlew --increment=patch --referenceTag=1.2.3 --tagMessage="Look mom, I'm tagging stuff!" incrementVersion`
* Output: `Incremented PATCH version from 1.2.3 to 1.2.4.`

## Pushing Tags
After you've incremented your project's version, you can either use git directly for pushing the new tags to remote, or you can do so using the `pushTags` task (also located in the `versioning` task group).
However, most git repositories will require you to authenticate for this step.
You can either set grgit credentials via system properties (`org.ajoberstar.grgit.auth.username` and `org.ajoberstar.grgit.auth.password`) or via environment variables (`GRGIT_USER` and `GRGIT_PASS`).
Read all about passing credentials to `Grgit` in the [official Grgit docs](https://ajoberstar.org/grgit/main/grgit-authentication.html).
### Example
If you want to use the `pushTags` task from a Jenkins pipeline, you can pass the credentials as follows:

```groovy
withCredentials([script.usernamePassword(credentialsId: 'foobar', usernameVariable: 'GRGIT_USER', passwordVariable: 'GRGIT_PASS')]) {
    sh './gradlew pushTags'
}
```
