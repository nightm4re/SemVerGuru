package de.gurkenlabs.semverguru

import org.ajoberstar.grgit.Grgit
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

abstract class PushTagsTask extends DefaultTask {

    @TaskAction
    def pushTags() {
        Grgit grgit = Grgit.open(dir: project.getProjectDir())
        grgit.push { tags = true }
        getLogger().lifecycle("Pushed all git tags to remote.")
    }
}
